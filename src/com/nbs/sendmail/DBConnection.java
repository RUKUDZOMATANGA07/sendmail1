package com.nbs.sendmail;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.io.*;
import static java.time.LocalDate.now;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Properties;

public class DBConnection {
	public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
		loadReportingPersonCBO();
	}

	public static void loadReportingPersonCBO() {
		Statement st = null;
		ResultSet rs = null;
		try {
			Connection con = getDBConnection();
			st = con.createStatement();
			String s = "SELECT CAST(bgfnam AS VARCHAR (500) CCSID 37) AS name,bgcnpr AS copname, bgcus  AS basic, BGEMAD as email , BGEMAL as email2 from kfilnbs.bgpf where bgcus='304781'";

			rs = st.executeQuery(s);
			int i = 1;
			while (rs.next()) {
				System.out.println(i++ + " name " + rs.getString("name") + " basic " + rs.getString("basic") + " email "
						+ rs.getString("email") + " ..");
			}
		} catch (Exception e) {
			System.out.println();
			e.printStackTrace();
		} finally {
			try {
				st.close();
				rs.close();
// con.close();
			} catch (Exception e) {
				System.out.print(e);
			}
		}
	}

	public static Connection getDBConnection() {

		Properties prop = new Properties();
		InputStream input = null;
		Connection dbConnection = null;

		String jdbcClassName = "com.ibm.as400.access.AS400JDBCDriver";
		String url = "jdbc:as400://192.168.1.33/B2059bbw";
		String user = "CSCP";
		String password = "CSCP";

		try {
			Class.forName(jdbcClassName);
			dbConnection = DriverManager.getConnection(url, user, password);
			System.out.println("connection successful...");
			return dbConnection;
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

}
